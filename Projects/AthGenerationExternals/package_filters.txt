# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
#
# List of packages to build as part of AthGenerationExternals.
#
+ External/CLHEP
+ External/FastJet
+ External/GPerfTools
+ External/Gdb
+ External/GeoModelCore
+ External/GoogleTest
+ External/HepMCAnalysis
+ External/MKL
+ External/PyModules
+ External/dSFMT
+ External/flake8_atlas
+ External/yampl
- .*

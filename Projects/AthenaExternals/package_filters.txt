# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
#
# List of packages to build as part of AthenaExternals.
#
+ External/Acts
+ External/CheckerGccPlugins
+ External/CLHEP
+ External/Coin3D
+ External/FastJet
+ External/flake8_atlas
+ External/GPerfTools
+ External/Gdb
+ External/Geant4
+ External/GeoModelCore
+ External/GeoModelIO
+ External/GeoModelTools
+ External/GoogleTest
+ External/HepMCAnalysis
+ External/lwtnn
+ External/MKL
+ External/onnxruntime
+ External/prmon
+ External/PyModules
+ External/Simage
+ External/SoQt
+ External/dSFMT
+ External/triSYCL
+ External/yampl
- .*

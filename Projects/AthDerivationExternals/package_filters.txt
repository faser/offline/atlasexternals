# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
#
# List of packages to build as part of AthDerivationExternals.
#
+ External/CheckerGccPlugins
+ External/CLHEP
+ External/FastJet
+ External/Gdb
+ External/GPerfTools
+ External/GoogleTest
+ External/MKL
+ External/dSFMT
+ External/lwtnn
+ External/onnxruntime
+ External/prmon
+ External/yampl
+ External/Lhapdf
- .*

# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
#
# Configuration for building LibXml2 as part of the offline / analysis release.
#

# Set the name of the package:
atlas_subdir( LibXml2 )

# In release recompilation mode stop now:
if( ATLAS_RELEASE_MODE )
   return()
endif()

# Stop if the build is not needed:
if( NOT ATLAS_BUILD_LIBXML2 )
   return()
endif()

# Tell the user what's happening:
message( STATUS "Building LibXml2 as part of this project" )

# The source code of LibXml2:
set( _source
   "http://cern.ch/lcgpackages/tarFiles/sources/libxml2-2.9.9.tar.gz" )
set( _md5 "c04a5a0a042eaa157e8e8c9eabe76bd6" )

# Temporary directory for the build results:
set( _buildDir
   ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/LibXml2Build )

# Create the sanitization script:
configure_file( ${CMAKE_CURRENT_SOURCE_DIR}/cmake/sanitizeConfig.sh.in
   ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/sanitizeConfig.sh @ONLY )

# Build LibXml2:
ExternalProject_Add( LibXml2
   PREFIX ${CMAKE_BINARY_DIR}
   INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   URL ${_source}
   URL_MD5 ${_md5}
   CONFIGURE_COMMAND <SOURCE_DIR>/configure --prefix=${_buildDir}
   --without-python --without-lzma --enable-shared --disable-static
   INSTALL_COMMAND make install
   COMMAND ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/sanitizeConfig.sh
   ${_buildDir}/bin/xml2-config ${_buildDir}/lib/pkgconfig/libxml-2.0.pc
   ${_buildDir}/lib/libxml2.la ${_buildDir}/lib/xml2Conf.sh
   COMMAND ${CMAKE_COMMAND} -E copy_directory ${_buildDir}/ <INSTALL_DIR> )
ExternalProject_Add_Step( LibXml2 purgeBuild
   COMMAND ${CMAKE_COMMAND} -E remove_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E make_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E remove_directory "${_buildDir}"
   COMMENT "Removing previous build results for LibXml2"
   DEPENDEES download
   DEPENDERS patch )
add_dependencies( Package_LibXml2 LibXml2 )

# Set up its installation:
install( DIRECTORY ${_buildDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )

# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
#
# Package building HDF5 for the offline/analysis releases.
#

# The name of the package:
atlas_subdir( HDF5 )

# Set the CMake version required by HDF5.
cmake_minimum_required( VERSION 3.10 )

# The source code of HDF5:
set( _source
   "https://cern.ch/lcgpackages/tarFiles/sources/hdf5-1.10.5.tar.gz" )
set( _md5 "e115eeb66e944fa7814482415dd21cc4" )

# Make sure that all _ROOT variables *are* used when they are set.
if( POLICY CMP0074 )
   cmake_policy( SET CMP0074 NEW )
endif()

# Temporary directory for the build results:
set( _buildDir "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/HDF5Build" )

# Find the external(s) needed for the build:
find_package( ZLIB )
list( GET ZLIB_INCLUDE_DIRS 0 _zlibInclude )
list( GET ZLIB_LIBRARIES    0 _zlibLibrary )

# Optional argument(s):
set( _extraConf )
if( NOT "${CMAKE_BUILD_TYPE}" STREQUAL "" )
   list( APPEND _extraConf -DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE} )
endif()
if( NOT "${CMAKE_CXX_STANDARD}" STREQUAL "" )
   list( APPEND _extraConf -DCMAKE_CXX_STANDARD:STRING=${CMAKE_CXX_STANDARD} )
endif()

# Build HDF5:
ExternalProject_Add( HDF5
   PREFIX ${CMAKE_BINARY_DIR}
   URL "${_source}"
   URL_MD5 "${_md5}"
   INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   CMAKE_CACHE_ARGS
   -DCMAKE_INSTALL_PREFIX:PATH=${_buildDir}
   -DHDF5_BUILD_CPP_LIB:BOOL=ON
   -DBUILD_SHARED_LIBS:BOOL=ON
   -DHDF5_ENABLE_Z_LIB_SUPPORT:BOOL=ON
   -DZLIB_LIBRARY:FILEPATH=${_zlibLibrary}
   -DZLIB_INCLUDE_DIR:PATH=${_zlibInclude}
   ${_extraConf}
   LOG_CONFIGURE 1 )
ExternalProject_Add_Step( HDF5 forcedownload
   COMMAND ${CMAKE_COMMAND} -E echo "Forcing the re-download of HDF5"
   DEPENDERS download )
ExternalProject_Add_Step( HDF5 purgeBuild
   COMMAND ${CMAKE_COMMAND} -E remove_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E make_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E remove_directory "${_buildDir}"
   COMMENT "Removing previous build results for HDF5"
   DEPENDEES download
   DEPENDERS patch )
ExternalProject_Add_Step( HDF5 buildinstall
   COMMAND ${CMAKE_COMMAND} -E remove -f ${_buildDir}/lib/libdynlib*
   COMMAND ${CMAKE_COMMAND} -E copy_directory ${_buildDir}/ <INSTALL_DIR>
   COMMENT "Installing HDF5 into the build area"
   DEPENDEES install )
add_dependencies( Package_HDF5 HDF5 )

# Install HDF5:
install( DIRECTORY ${_buildDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )

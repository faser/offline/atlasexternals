# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
#
# Package building Acts as part of the offline / analysis software build.
#

# The name of the package:
atlas_subdir( Acts )

# External(s) needed by Acts:
find_package( Eigen3 )
find_package( Boost 1.62.0 )
find_package( nlohmann_json )

# In release recompilation mode stop here:
if( ATLAS_RELEASE_MODE )
   return()
endif()

# Tell the user what's happening:
message( STATUS "Building Acts as part of this project" )

# Temporary directory for the build results:
set( _buildDir ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/ActsBuild )
# Directory holding the "stamp" files:
set( _stampDir ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/ActsStamp )

# List of paths given to CMAKE_PREFIX_PATH.
set( _prefixPaths ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   $ENV{CMAKE_PREFIX_PATH} ${BOOST_LCGROOT} ${Eigen3_DIR} ${nlohmann_json_DIR} )

# Extra configuration options for Acts:
set( _extraOptions )
if( "${CMAKE_BUILD_TYPE}" STREQUAL "Release" OR
      "${CMAKE_BUILD_TYPE}" STREQUAL "RelWithDebInfo" )
   list( APPEND _extraOptions -DCMAKE_BUILD_TYPE:STRING=Release )
elseif( NOT "${CMAKE_BUILD_TYPE}" STREQUAL "" )
   list( APPEND _extraOptions -DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE} )
endif()

# Set the repository and tag to be used through cache variables.
set( ATLAS_ACTS_REPOSITORY "https://github.com/acts-project/acts.git"
   CACHE STRING "Repository to fetch Acts from" )
set( ATLAS_ACTS_TAG "v0.31.00"
   CACHE STRING "Git tag to use for the Acts build" )

# Decide whether / how to patch the Acts sources.
set( _patchCommand )
# No patches currently needed.

# Build Acts for the build area:
ExternalProject_Add( Acts
   PREFIX ${CMAKE_BINARY_DIR}
   INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   STAMP_DIR ${_stampDir}
   GIT_REPOSITORY "${ATLAS_ACTS_REPOSITORY}"
   GIT_TAG "${ATLAS_ACTS_TAG}"
   ${_patchCommand}
   CMAKE_CACHE_ARGS
   -DCMAKE_INSTALL_LIBDIR:PATH=${CMAKE_INSTALL_LIBDIR}
   -DCMAKE_INSTALL_PREFIX:PATH=${_buildDir}
   -DCMAKE_CXX_STANDARD:STRING=${CMAKE_CXX_STANDARD}
   -DCMAKE_CXX_EXTENSIONS:BOOL=${CMAKE_CXX_EXTENSIONS}
   -DACTS_BUILD_JSON_PLUGIN:BOOL=ON
   -DACTS_USE_BUNDLED_NLOHMANN_JSON:BOOL=OFF # do not build separately
   -DCMAKE_PREFIX_PATH:PATH=${_prefixPaths}
   ${_extraOptions}
   LOG_CONFIGURE 1 )
ExternalProject_Add_Step( Acts cleansource
   COMMAND ${CMAKE_COMMAND} -E remove -f
   "${_stampDir}/Acts-gitclone-lastrun.txt"
   DEPENDERS download )
# Need to modify the printout here when the Acts version is updated, and we want
# to ensure a clean build.
ExternalProject_Add_Step( Acts forcedownload
   COMMAND ${CMAKE_COMMAND} -E echo "Forcing the re-download of Acts (13.06.2020)"
   DEPENDERS cleansource )
ExternalProject_Add_Step( Acts purgeBuild
   COMMAND ${CMAKE_COMMAND} -E remove_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E make_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E remove_directory "${_buildDir}"
   COMMENT "Removing previous build results for Acts"
   DEPENDEES download
   DEPENDERS patch )
ExternalProject_Add_Step( Acts forceconfigure
   COMMAND ${CMAKE_COMMAND} -E remove -f <BINARY_DIR>/CMakeCache.txt
   COMMENT "Forcing the configuration of Acts"
   DEPENDEES update
   DEPENDERS configure
   ALWAYS 1 )
ExternalProject_Add_Step( Acts buildinstall
   COMMAND ${CMAKE_COMMAND} -E copy_directory ${_buildDir}/ <INSTALL_DIR>
   COMMENT "Installing Acts into the build area"
   DEPENDEES install )
add_dependencies( Package_Acts Acts )
add_dependencies( Acts nlohmann_json )

# And now install it:
install( DIRECTORY ${_buildDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )

ACTS, A Common Tracking Software
================================

This package is used to build [ACTS](http://acts.web.cern.ch/ACTS/) for any
ATLAS release that needs it.
